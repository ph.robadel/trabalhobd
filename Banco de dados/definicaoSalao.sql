-- CREATE DATABASE salao;
-- USE salao;

CREATE TABLE Cargo(
	idCargo INTEGER NOT NULL,
	descricao VARCHAR(100) NOT NULL,
	salario DOUBLE NOT NULL, 
	
	CONSTRAINT pkCargo PRIMARY KEY (idCargo)
);

CREATE TABLE Funcionario(
	idFuncionario INTEGER NOT NULL,
	idCargo INTEGER NOT NULL,
	nome VARCHAR(100) NOT NULL,
	cpf VARCHAR(14) NOT NULL, 
	telefone VARCHAR(14) NOT NULL,
	dataEntrada DATE NOT NULL,
	dataSaida DATE,
	
	CONSTRAINT pkFuncionario PRIMARY KEY (idFuncionario),
	CONSTRAINT fkFuncionarioCargo FOREIGN KEY(idCargo) REFERENCES Cargo(idCargo)
);

CREATE TABLE Atendente(
	idAtendente INTEGER NOT NULL,
	login VARCHAR(50) NOT NULL,
	senha VARCHAR(50) NOT NULL,
	statusLogin CHAR(2) NOT NULL, 
	
	CONSTRAINT pkAtendente PRIMARY KEY (idAtendente),
	CONSTRAINT fkFuncionarioAtendente FOREIGN KEY(idAtendente) REFERENCES Funcionario(idFuncionario) 
);

CREATE TABLE Servico(
 	idServico INTEGER NOT NULL,
 	codigo INTEGER NOT NULL, 
	descricao VARCHAR(100) NOT NULL,
	tempoMedio INTEGER NOT NULL,
	valor DOUBLE NOT NULL,	
	
 	CONSTRAINT pkServico PRIMARY KEY (idServico)
);

CREATE TABLE FuncionarioServico(
	idFuncionario INTEGER NOT NULL,
 	idServico INTEGER NOT NULL,
	
 	CONSTRAINT pkFuncionarioServico PRIMARY KEY (idFuncionario, idServico),
	CONSTRAINT fkFuncionarioServicoFuncionario FOREIGN KEY (idFuncionario) REFERENCES Funcionario(idFuncionario),
 	CONSTRAINT fkFuncionarioServicoServico FOREIGN KEY (idServico) REFERENCES Servico(idServico)
);

CREATE TABLE Cliente(
 	idCliente INTEGER NOT NULL,
 	nome VARCHAR(100) NOT NULL,
	cpf VARCHAR(14) NOT NULL,
 	telefone VARCHAR(14) NOT NULL,
 	idade INTEGER NOT NULL,
	sexo CHAR(1) NOT NULL,
	
 	CONSTRAINT pkCliente PRIMARY KEY (idCliente)
);

CREATE TABLE Agendamento(
 	idAgendamento INTEGER NOT NULL,
	idFuncionario INTEGER NOT NULL,
	idServico INTEGER NOT NULL,
	idCliente INTEGER NOT NULL,
	idAtendente INTEGER NOT NULL,
	dataAgendamento DATE NOT NULL,
	horarioAgendamento TIME NOT NULL,
	dataAtendimento DATE NOT NULL,
	horarioAtendimento TIME NOT NULL,
	status VARCHAR(50),
	
 	CONSTRAINT pkAgendamento PRIMARY KEY (idAgendamento),
	CONSTRAINT fkAgendamentoFuncionarioServico FOREIGN KEY (idFuncionario, idServico) REFERENCES FuncionarioServico(idFuncionario, idServico),
	CONSTRAINT fkAgendamentoCliente FOREIGN KEY (idCliente) REFERENCES Cliente(idCliente),
 	CONSTRAINT fkAgendamentoAtendente FOREIGN KEY (idAtendente) REFERENCES Atendente(idAtendente)
);

CREATE TABLE Pagamento(
 	idPagamento INTEGER NOT NULL,
 	idAtendente INTEGER NOT NULL,
	idAgendamento INTEGER NOT NULL,
 	formaPagamento VARCHAR (50) NOT NULL,
 	valor DOUBLE NOT NULL, 
	dataPagamento DATE NOT NULL,
	horaPagamento TIME NOT NULL,
	
 	CONSTRAINT pkPagamento PRIMARY KEY (idPagamento),
 	CONSTRAINT fkPagamentoAtendente FOREIGN KEY (idAtendente) REFERENCES Atendente(idAtendente),
	CONSTRAINT fkPagamentoAgendamento FOREIGN KEY (idAgendamento) REFERENCES Agendamento(idAgendamento)
);


















