package model;

public class Cargo {
    private int idCargo;
    private String descricao;
    private double salario;

    public Cargo() {
    }

    public Cargo(String descricao, double salario) {
        this.descricao = descricao;
        this.salario = salario;
    }

    public Cargo(int idCargo, String descricao, double salario) {
        this.idCargo = idCargo;
        this.descricao = descricao;
        this.salario = salario;
    }

    @Override
    public String toString() {
        return "Cargo{" + "idCargo=" + idCargo + ", descricao=" + descricao + ", salario=" + salario + '}';
    }
    
    public String getXML(){
        String xml = "";
        xml+= "\t\t<cargo>\n";
        xml+= "\t\t\t<op>l</op>\n";
        xml+= "\t\t\t<id>"+idCargo+"</id>\n";
        xml+= "\t\t\t<descricao>"+descricao+"</descricao>\n";
        xml+= "\t\t\t<salario>"+salario+"</salario>\n";
        xml+= "\t\t</cargo>\n";
        return xml;
    }


    public int getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    
}
