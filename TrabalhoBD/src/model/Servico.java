package model;

public class Servico {
    private int idServico;
    private int codigo;
    private String descricao;
    private int tempoMedio;
    private double valor;

    public Servico() {
    }

    public Servico(int codigo, String descricao, int tempoMedio, double valor) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.tempoMedio = tempoMedio;
        this.valor = valor;
    }

    public Servico(int idServico, int codigo, String descricao, int tempoMedio, double valor) {
        this.idServico = idServico;
        this.codigo = codigo;
        this.descricao = descricao;
        this.tempoMedio = tempoMedio;
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Servico{" + "idServico=" + idServico + ", codigo=" + codigo + ", descricao=" + descricao + ", tempoMedio=" + tempoMedio + ", valor=" + valor + '}';
    }
    
    public String getXML(){
        String xml = "";
        xml+= "\t\t<servico>\n";
        xml+= "\t\t\t<op>l</op>\n";
        xml+= "\t\t\t<id>"+idServico+"</id>\n";
        xml+= "\t\t\t<codigo>"+codigo+"</codigo>\n";
        xml+= "\t\t\t<descricao>"+descricao+"</descricao>\n";
        xml+= "\t\t\t<tempoMedio>"+tempoMedio+"</tempoMedio>\n";
        xml+= "\t\t\t<valor>"+valor+"</valor>\n";
        xml+= "\t\t</servico>\n";
        return xml;
    }

    public int getIdServico() {
        return idServico;
    }

    public void setIdServico(int idServico) {
        this.idServico = idServico;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getTempoMedio() {
        return tempoMedio;
    }

    public void setTempoMedio(int tempoMedio) {
        this.tempoMedio = tempoMedio;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
    
    
}
