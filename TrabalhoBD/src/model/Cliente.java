package model;

public class Cliente {
    private int idCliente;
    private String nome;
    private String cpf;
    private String telefone;
    private int idade;
    private String sexo;

    public Cliente() {
    }

    public Cliente(String nome, String cpf, String telefone, int idade, String sexo) {
        this.nome = nome;
        this.cpf = cpf;
        this.telefone = telefone;
        this.idade = idade;
        this.sexo = sexo;
    }

    public Cliente(int idCliente, String nome, String cpf, String telefone, int idade, String sexo) {
        this.idCliente = idCliente;
        this.nome = nome;
        this.cpf = cpf;
        this.telefone = telefone;
        this.idade = idade;
        this.sexo = sexo;
    }

    @Override
    public String toString() {
        return "Cliente{" + "idCliente=" + idCliente + ", nome=" + nome + ", cpf=" + cpf + ", telefone=" + telefone + ", idade=" + idade + ", sexo=" + sexo + '}';
    }
    
    public String getXML(){
        String xml = "";
        xml+= "\t\t<cliente>\n";
        xml+= "\t\t\t<op>l</op>\n";
        xml+= "\t\t\t<id>"+idCliente+"</id>\n";
        xml+= "\t\t\t<nome>"+nome+"</nome>\n";
        xml+= "\t\t\t<cpf>"+cpf+"</cpf>\n";
        xml+= "\t\t\t<telefone>"+telefone+"</telefone>\n";
        xml+= "\t\t\t<idade>"+idade+"</idade>\n";
        xml+= "\t\t\t<sexo>"+sexo+"</sexo>\n";
        xml+= "\t\t</cliente>\n";
        return xml;
    }
    
    
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    
    
    
}
