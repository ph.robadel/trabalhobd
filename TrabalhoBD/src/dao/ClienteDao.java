package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Cliente;

public class ClienteDao {
    private Connection conn;

    public ClienteDao(int banco) throws Exception {
         if(banco == Conexao.MYSQL){
            try {
                this.conn = Conexao.getConexaoMysql();
            } catch (Exception e) {
                throw new Exception("Erro: \n" + e.getMessage());
            }
        }
        
        else if(banco == Conexao.SQLITE){
            try {
                this.conn = Conexao.getConexaoSqlite();
            } catch (Exception e) {
                throw new Exception("Erro: \n" + e.getMessage());
            }
        }
    }
    
    public ClienteDao(Connection c) {
        this.conn = c;
    }
    
    public void salvar(Cliente cliente) throws Exception {
        PreparedStatement ps = null;

        if (cliente == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {
                      
            String SQL = "INSERT INTO Cliente (idCliente, nome, cpf,telefone,idade,sexo) values (?, ?, ?, ?, ?, ?)";

            ps = conn.prepareStatement(SQL);
            ps.setInt(1, cliente.getIdCliente());
            ps.setString(2, cliente.getNome());
            ps.setString(3, cliente.getCpf());
            ps.setString(4, cliente.getTelefone());
            ps.setInt(5, cliente.getIdade());
            ps.setString(6, cliente.getSexo());
            ps.executeUpdate();

        } catch (java.sql.SQLIntegrityConstraintViolationException sqle) {
            throw new java.sql.SQLIntegrityConstraintViolationException(sqle);
        } catch (SQLException sqle) {
            throw new SQLException(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }

    public void atualizar(Cliente cliente) throws Exception{
        PreparedStatement ps = null;
        
        if (cliente == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {
            String SQL = "UPDATE Cliente SET nome = ?, cpf = ?,telefone = ?,idade = ?,sexo = ?"
                    + "where idCliente = ?";
            
            ps = conn.prepareStatement(SQL);
            
            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getCpf());
            ps.setString(3, cliente.getTelefone());
            ps.setInt(4, cliente.getIdade());
            ps.setString(5, cliente.getSexo());
            ps.setInt(6, cliente.getIdCliente());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ps.close();
        }
    }
    
    public void excluir(Cliente cliente) throws Exception {
        PreparedStatement ps = null;
        
        if (cliente == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {            
            ps = conn.prepareStatement("delete from Cliente where idCliente = ?");
            ps.setInt(1, cliente.getIdCliente());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new SQLException(sqle);
        } finally {
            ps.close();
        }
    }
    
    public boolean isClienteExiste(int id) throws Exception {
        PreparedStatement ps = null;
        
        ResultSet rs = null;
        try {            
            ps = conn.prepareStatement("select * from Cliente where idCliente = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            
            if (rs.next())
                return true;
            
            
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            rs.close();
            ps.close();            
        }
        return false;
    }
    
    public ArrayList<Cliente> todosClientes() throws Exception {
        PreparedStatement ps = null;
        
        ResultSet rs = null;
        try {
            
            ps = conn.prepareStatement("select * from Cliente");
            rs = ps.executeQuery();
            
            ArrayList<Cliente> clientes = new ArrayList<>();
            
            while (rs.next()) {
                
                int id = rs.getInt(1);
                String nome = rs.getString(2);
                String cpf = rs.getString(3);
                String telefone = rs.getString(4);
                int idade = rs.getInt(5);
                String sexo = rs.getString(6);
                clientes.add(new Cliente(id,nome,cpf,telefone,idade,sexo));

            }
            
            return clientes;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
            
        }
    }
}
