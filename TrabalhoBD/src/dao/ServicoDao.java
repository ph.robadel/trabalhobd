package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Servico;

public class ServicoDao {
    private Connection conn;

    public ServicoDao(int banco) throws Exception {
        if(banco == Conexao.MYSQL){
            try {
                this.conn = Conexao.getConexaoMysql();
            } catch (Exception e) {
                throw new Exception("Erro: \n" + e.getMessage());
            }
        }
        
        else if(banco == Conexao.SQLITE){
            try {
                this.conn = Conexao.getConexaoSqlite();
            } catch (Exception e) {
                throw new Exception("Erro: \n" + e.getMessage());
            }
        }
    }
    
    public ServicoDao(Connection c) {
        this.conn = c;
    }
    
    public void salvar(Servico servico) throws Exception {
        PreparedStatement ps = null;

        if (servico == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {
                      
            String SQL = "INSERT INTO Servico (idServico, codigo, descricao,tempoMedio,valor) values (?, ?, ?, ?, ?)";

            ps = conn.prepareStatement(SQL);
            ps.setInt(1, servico.getIdServico());
            ps.setInt(2, servico.getCodigo());
            ps.setString(3, servico.getDescricao());
            ps.setInt(4, servico.getTempoMedio());
            ps.setDouble(5, servico.getValor());
            ps.executeUpdate();

        } catch (java.sql.SQLIntegrityConstraintViolationException sqle) {
            throw new java.sql.SQLIntegrityConstraintViolationException(sqle);
        } catch (SQLException sqle) {
            throw new SQLException(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }

    public void atualizar(Servico servico) throws Exception{
        PreparedStatement ps = null;
        
        if (servico == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {
            String SQL = "UPDATE Servico SET idServico = ?, codigo = ?, descricao = ?, tempoMedio = ?, valor = ?"
                    + "where idServico = ?";
            
            ps = conn.prepareStatement(SQL);
            
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, servico.getIdServico());
            ps.setInt(2, servico.getCodigo());
            ps.setString(3, servico.getDescricao());
            ps.setInt(4, servico.getTempoMedio());
            ps.setDouble(5, servico.getValor());
            ps.setInt(6, servico.getIdServico());
            
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar Servico: " + sqle);
        } finally {
            ps.close();
        }
    }
    
    public void excluir(Servico servico) throws Exception {
        PreparedStatement ps = null;
        
        if (servico == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {            
            ps = conn.prepareStatement("delete from Servico where idServico = ?");
            ps.setInt(1, servico.getIdServico());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new SQLException(sqle);
        } finally {
            ps.close();
        }
    }
    
    public boolean isServicoExiste(int id) throws Exception {
        PreparedStatement ps = null;
        
        ResultSet rs = null;
        try {            
            ps = conn.prepareStatement("select * from Servico where idServico = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            
            if (rs.next())
                return true;
            
            
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            rs.close();
            ps.close();            
        }
        return false;
    }
    
    public ArrayList<Servico> todosServicos() throws Exception {
        PreparedStatement ps = null;
        
        ResultSet rs = null;
        try {
            
            ps = conn.prepareStatement("select * from Servico");
            rs = ps.executeQuery();
            
            ArrayList<Servico> servicos = new ArrayList<>();
            
            while (rs.next()) {
                
                int id = rs.getInt(1);
                int codigo = rs.getInt(2);
                String descricao = rs.getString(3);
                int tempoMedio = rs.getInt(4);
                double valor = rs.getDouble(5);
                servicos.add(new Servico(id,codigo,descricao,tempoMedio,valor));

            }
            
            return servicos;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
            
        }
    }
}
