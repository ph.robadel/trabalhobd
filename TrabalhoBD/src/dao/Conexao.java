package dao;

import java.sql.*;

public class Conexao{
    
    public static final int MYSQL = 0;
    public static final int SQLITE = 1;
    
    public static Connection getConexaoMysql(){
        Connection con = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/salao","root", "123456");
        }
        catch (Exception e)
        {
            System.out.println("falha na conexão: "+e);
        }
            return con;
    }
    
    public static Connection getConexaoSqlite(){
        Connection con = null;
        try{
            con = DriverManager.getConnection("jdbc:sqlite:sqlite3/base.salao");
        }
        catch (Exception e){
            System.out.println("falha na conexão: "+e);
        }
        return con;
    }

public static void fecharConexao(Connection conn, Statement stmt, ResultSet rs){
    try {
        if (rs != null) rs.close( );
        if (stmt != null) stmt.close( );
        if (conn != null) conn.close( );
    } catch (Exception e) { }
 }

public static void fecharConexao(Connection conn){
    try {
        if (conn != null) conn.close( );
    } catch (Exception e) { }
}
public static void fecharConexao(Connection conn,PreparedStatement ps)
{
    try {
        if (ps != null) ps.close( );
        if (conn != null) conn.close( );
    } catch (Exception e) { }
}

}

