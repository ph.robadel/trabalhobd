package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Cargo;

public class CargoDao {
    private Connection conn;

    public CargoDao(int banco) throws Exception {
         if(banco == Conexao.MYSQL){
            try {
                this.conn = Conexao.getConexaoMysql();
            } catch (Exception e) {
                throw new Exception("Erro: \n" + e.getMessage());
            }
        }
        
        else if(banco == Conexao.SQLITE){
            try {
                this.conn = Conexao.getConexaoSqlite();
            } catch (Exception e) {
                throw new Exception("Erro: \n" + e.getMessage());
            }
        }
    }
    
    public CargoDao(Connection c) {
        this.conn = c;
    }
    
    public void salvar(Cargo cargo) throws Exception {
        PreparedStatement ps = null;

        if (cargo == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {
                      
            String SQL = "INSERT INTO Cargo (idCargo, descricao, salario) values (?, ?, ?)";

            ps = conn.prepareStatement(SQL);
            ps.setInt(1, cargo.getIdCargo());
            ps.setString(2, cargo.getDescricao());
            ps.setDouble(3, cargo.getSalario());
            ps.executeUpdate();

        } catch (java.sql.SQLIntegrityConstraintViolationException sqle) {
            throw new java.sql.SQLIntegrityConstraintViolationException(sqle);
        } catch (SQLException sqle) {
            throw new SQLException(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps);
        }
    }

    public void atualizar(Cargo cargo) throws Exception{
        PreparedStatement ps = null;
        
        if (cargo == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {
            String SQL = "UPDATE Cargo SET descricao=?, salario=?"
                    + "where idCargo = ?";
            
            ps = conn.prepareStatement(SQL);
            
            ps.setString(1, cargo.getDescricao());
            ps.setDouble(2, cargo.getSalario());
            ps.setInt(3, cargo.getIdCargo());
            
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar Cargo: " + sqle);
        } finally {
            ps.close();
        }
    }
    
    public void excluir(Cargo cargo) throws Exception {
        PreparedStatement ps = null;
        
        if (cargo == null) {
            throw new NullPointerException("O valor passado não pode ser nulo");
        }
        try {            
            ps = conn.prepareStatement("delete from Cargo where idCargo = ?");
            ps.setInt(1, cargo.getIdCargo());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new SQLException(sqle);
        } finally {
            ps.close();
        }
    }
    
    public boolean isCargoExiste(int id) throws Exception {
        PreparedStatement ps = null;
        
        ResultSet rs = null;
        try {            
            ps = conn.prepareStatement("select * from Cargo where idCargo = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            
            if (rs.next())
                return true;
            
            
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            rs.close();
            ps.close();            
        }
        return false;
    }
    
    public ArrayList<Cargo> todosCargos() throws Exception {
        PreparedStatement ps = null;
        
        ResultSet rs = null;
        try {
            
            ps = conn.prepareStatement("select * from Cargo");
            rs = ps.executeQuery();
            
            ArrayList<Cargo> cargos = new ArrayList<>();
            
            while (rs.next()) {
                
                int id = rs.getInt(1);
                String descricao = rs.getString(2);
                Double salario = rs.getDouble(3);
                cargos.add(new Cargo(id,descricao,salario));

            }
            
            return cargos;
        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            Conexao.fecharConexao(conn, ps, rs);
            
        }
    }
}
